﻿using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public class CustomerGrpcService: CustomersInfo.CustomersInfoBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomerGrpcService(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить всех клиентов
        /// </summary>
        /// <param name="request"></param>
        /// <param name="responseStream"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public async override Task GetCustomers(EmptyRequest request, IServerStreamWriter<CustomerAnswer> responseStream, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();
            foreach (var cust in customers)
            {
                await responseStream.WriteAsync(new CustomerAnswer 
                {  
                    Id = cust.Id.ToString(), 
                    Email = cust.Email, 
                    FirstName = cust.FirstName, 
                    LastName = cust.LastName 
                });
            }
        }

        /// <summary>
        /// Получить данные клинта по Id
        /// </summary>
        /// <param name="request"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public async override Task<CustomerResponce> GetCustomerById(IdRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            var responce =  new CustomerResponce()
            {
                 Id = customer.Id.ToString(),
                 Email = customer.Email,
                 FirstName = customer.FirstName,
                 LastName= customer.LastName
            };
            var preferences = customer.Preferences
                .Select(p => new PreferenceMessageResponce { Id = p.PreferenceId.ToString(), Name = p.Preference.Name })
                .ToList(); 
            responce.Preferences.Add(preferences);
            return responce;
        }

        /// <summary>
        /// Добавить нового клиента
        /// </summary>
        /// <param name="request"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public async override Task<CustomerResponce> CreateCustomer(CreateOrEditCustomerMessageRequest request, ServerCallContext context)
        {
            var preferences = await _preferenceRepository
               .GetRangeByIdsAsync(request.PreferenceIds.Select(id=>Guid.Parse(id)).ToList());

            Customer customer = CustomerMapper.MapFromMessage(request, preferences);

            await _customerRepository.AddAsync(customer);

            return await GetCustomerById(new IdRequest { Id = customer.Id.ToString() }, context);
        }





    }
}
