﻿using HotChocolate;
using HotChocolate.Data;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL
{
    /// <summary>
    /// В этом классе будут находится методы для аналгов GET 
    /// </summary>
    public class CustomerQueries
    {
        private readonly IRepository<Customer> _customerRepository;

        public CustomerQueries(IRepository<Customer> customerRepository)
        {
            _customerRepository = customerRepository;
        }

        [UseFiltering()]
        [UseSorting()]
        public async Task<List<CustomerShortResponse>> GetCustomers()
        {
            var customers = await _customerRepository.GetAllAsync();
            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();
            return response;
        }

        /// <summary>
        /// Метод излишний, поскольку фильтрация позволяет делать выборку
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<CustomerResponse> GetCustomerById(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if(customer is null) throw new ArgumentException($"Customer with id=={id} не найден");
            return new CustomerResponse(customer);
        }




    }
}
