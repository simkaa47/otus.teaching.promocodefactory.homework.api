﻿using Microsoft.Extensions.DependencyInjection;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Extentions
{
    public static class ServiceExtentions
    {
        public static IServiceCollection AddGrpcApi(this IServiceCollection services)
        {
            if(services==null)services=new ServiceCollection();
            services.AddGrpc();
            services.AddGrpcReflection();            
            return services;
        }
    }
}
